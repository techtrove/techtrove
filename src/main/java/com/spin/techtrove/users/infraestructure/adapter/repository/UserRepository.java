package com.spin.techtrove.users.infraestructure.adapter.repository;

import com.spin.techtrove.users.infraestructure.adapter.entity.UserE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserE, Long> {

    List<UserE> findByIdIn(List<Long> catsIds);
    Optional<UserE> findByUser(String user);

}