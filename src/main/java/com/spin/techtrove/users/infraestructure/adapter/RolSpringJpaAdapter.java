package com.spin.techtrove.users.infraestructure.adapter;

import com.spin.techtrove.users.domain.model.Rol;
import com.spin.techtrove.users.domain.port.RolPersistencePort;
import com.spin.techtrove.users.infraestructure.adapter.entity.RolE;
import com.spin.techtrove.users.infraestructure.adapter.mapper.RolDboMapper;
import com.spin.techtrove.users.infraestructure.adapter.repository.RolRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RolSpringJpaAdapter implements RolPersistencePort {
    private final RolRepository rolRepository;
    private final RolDboMapper rolDboMapper;
    public RolSpringJpaAdapter(RolRepository rolRepository, RolDboMapper rolDboMapper) {
        this.rolDboMapper = rolDboMapper;
        this.rolRepository = rolRepository;
    }

    @Override
    public List<Rol> create(List<Rol> request) {

        List<RolE> roles = request.stream()
                .map(rolDboMapper::toDbo)
                .collect(Collectors.toList());

        rolRepository.saveAll(roles);

        return request;

    }

}
