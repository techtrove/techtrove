package com.spin.techtrove.users.infraestructure.adapter.config;

import java.util.Date;

import com.spin.techtrove.users.infraestructure.adapter.entity.UserE;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenUtil {
    private static final long EXPIRE_DURATION = 24 * 60 * 60 * 1000; // 24 hour

    @Value("${app.jwt.secret}")
    private String SECRET_KEY;

    public String generateAccessToken(UserE user) {
        return Jwts.builder()
                .setSubject(String.format("%s,%s", user.getUser(), user.getPass()))
                .setIssuer("Test")
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRE_DURATION))
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();

    }

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenUtil.class);

    public boolean validateAccessToken(String token) {
        try {
            Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token);
            return true;
        } catch (ExpiredJwtException ex) {
            LOGGER.error("JWT Expiro", ex.getMessage());
        } catch (IllegalArgumentException ex) {
            LOGGER.error("Token es nulo o vacio", ex.getMessage());
        } catch (MalformedJwtException ex) {
            LOGGER.error("JWT invalido", ex);
        } catch (UnsupportedJwtException ex) {
            LOGGER.error("JWT no es soportado", ex);
        } catch (SignatureException ex) {
            LOGGER.error("Validacion de firma fallida.");
        }

        return false;
    }

    public String getSubject(String token) {
        return parseClaims(token).getSubject();
    }

    private Claims parseClaims(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
    }
}