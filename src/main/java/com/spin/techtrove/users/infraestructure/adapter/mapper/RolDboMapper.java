package com.spin.techtrove.users.infraestructure.adapter.mapper;

import com.spin.techtrove.users.domain.model.Rol;
import com.spin.techtrove.users.infraestructure.adapter.entity.RolE;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RolDboMapper {

    //@Mapping(source = "id", target = "id")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "user", target = "user")
    RolE toDbo(Rol domain);

    @InheritInverseConfiguration
    Rol toDomain(RolE entity);
}