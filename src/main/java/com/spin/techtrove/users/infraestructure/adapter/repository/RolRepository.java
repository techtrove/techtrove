package com.spin.techtrove.users.infraestructure.adapter.repository;

import com.spin.techtrove.users.infraestructure.adapter.entity.RolE;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RolRepository extends JpaRepository<RolE, Long> {
}
