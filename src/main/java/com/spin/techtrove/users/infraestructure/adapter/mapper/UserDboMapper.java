package com.spin.techtrove.users.infraestructure.adapter.mapper;

import com.spin.techtrove.users.domain.model.User;
import com.spin.techtrove.users.infraestructure.adapter.entity.UserE;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserDboMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "user", target = "user")
    @Mapping(source = "pass", target = "pass")
    @Mapping(source = "name", target = "name")
    UserE toDbo(User domain);

    @InheritInverseConfiguration
    User toDomain(UserE entity);
}