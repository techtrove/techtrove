package com.spin.techtrove.users.infraestructure.adapter;

import com.spin.techtrove.products.infraestructure.adapter.exception.CategoryException;
import com.spin.techtrove.users.domain.model.User;
import com.spin.techtrove.users.domain.model.constant.UserConstant;
import com.spin.techtrove.users.domain.port.UserPersistencePort;
import com.spin.techtrove.users.infraestructure.adapter.entity.RolE;
import com.spin.techtrove.users.infraestructure.adapter.entity.UserE;
import com.spin.techtrove.users.infraestructure.adapter.mapper.UserDboMapper;
import com.spin.techtrove.users.infraestructure.adapter.mapper.RolDboMapper;
import com.spin.techtrove.users.infraestructure.adapter.repository.RolRepository;
import com.spin.techtrove.users.infraestructure.adapter.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserSpringJpaAdapter implements UserPersistencePort {
    private final UserRepository userRepository;
    private final RolRepository rolRepository;
    private final UserDboMapper userDboMapper;
    private final RolDboMapper rolDboMapper;

    public UserSpringJpaAdapter(UserRepository userRepository, UserDboMapper userDboMapper, RolDboMapper rolDboMapper, RolRepository rolRepository ) {
        this.userDboMapper = userDboMapper;
        this.userRepository = userRepository;
        this.rolDboMapper = rolDboMapper;
        this.rolRepository = rolRepository;
    }

    @Override
    public User create(User request) {

        var userToSave = userDboMapper.toDbo(request);
        var userSaved = userRepository.save(userToSave);

        Set<RolE> rolesItems = request.getRoles().stream()
                .map(item -> {
                    var rol = rolDboMapper.toDbo(item);
                    rol.setUser(userSaved);
                    return rol;
                })
                .collect(Collectors.toSet());

        rolRepository.saveAll(rolesItems);

        var userDomain = userDboMapper.toDomain(userSaved);
        userDomain.setRoles(request.getRoles());

        return userDomain;

    }

    @Override
    public User getById(Long id) {
        var optionalUser = userRepository.findById(id);

        if (optionalUser.isEmpty()){
            throw new CategoryException(HttpStatus.NOT_FOUND, String.format(UserConstant.USER_NOT_FOUND_MESSAGE_ERROR, id));
        }

        return userDboMapper.toDomain(optionalUser.get());
    }

    @Override
    public List<User> getAll() {
        List<UserE> users = userRepository.findAll();
        UserE userE = users.get(0);
        User user = userDboMapper.toDomain(userE);

        return userRepository.findAll()
                .stream()
                .map(userDboMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User update(User user) {

        var rolToUpdate = userDboMapper.toDbo(user);
        var rolUpdated = userRepository.save(rolToUpdate);

        return userDboMapper.toDomain(rolUpdated);

    }

}
