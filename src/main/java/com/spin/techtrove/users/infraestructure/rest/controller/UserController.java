package com.spin.techtrove.users.infraestructure.rest.controller;

import com.spin.techtrove.users.application.usecases.UserService;
import com.spin.techtrove.users.domain.model.User;
import com.spin.techtrove.users.domain.model.dto.UserDto;
import com.spin.techtrove.users.domain.model.dto.request.UserRequest;
import com.spin.techtrove.users.infraestructure.adapter.config.JwtTokenUtil;
import com.spin.techtrove.users.infraestructure.adapter.entity.UserE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    @Autowired
    AuthenticationManager authManager;

    @Autowired
    JwtTokenUtil jwtUtil;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/obtener/{id}")
    public UserDto getById(@PathVariable Long id){
        return userService.getById(id);
    }

    @GetMapping
    public List<UserDto> getAll() {
        return userService.getAll();
    }

    @PostMapping("/login")
    public UserDto login(@RequestBody UserRequest userRequest){

        Authentication authentication = authManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userRequest.getUser(), userRequest.getPass()
                ));

        UserE user = (UserE) authentication.getPrincipal();
        String accessToken = jwtUtil.generateAccessToken(user);
        UserDto response = new UserDto();

        response.setName(user.getName());
        response.setUser(user.getUser());
        response.setToken(accessToken);

        return response;
    }

    @PostMapping("/registrar")
    public UserDto create(@RequestBody UserRequest userRequest){

        return userService.createNew(userRequest);
    }

    @PutMapping("/actualizar/{id}")
    public UserDto edit(@RequestBody UserRequest userRequest,
                               @PathVariable Long id){
        return userService.update(userRequest, id);
    }

    @DeleteMapping("/borrar/{id}")
    public void deleteById(@PathVariable Long id){
        userService.deleteById(id);
    }

}
