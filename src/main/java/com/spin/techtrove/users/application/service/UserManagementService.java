package com.spin.techtrove.users.application.service;

import com.spin.techtrove.users.application.mapper.UserDtoMapper;
import com.spin.techtrove.users.application.mapper.UserRequestMapper;
import com.spin.techtrove.users.application.usecases.UserService;
import com.spin.techtrove.users.domain.model.dto.UserDto;
import com.spin.techtrove.users.domain.model.dto.request.UserRequest;
import com.spin.techtrove.users.domain.port.UserPersistencePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserManagementService implements UserService {

    private final UserPersistencePort userPersistencePort;
    private final UserRequestMapper userRequestMapper;
    private final UserDtoMapper userDtoMapper;

    @Autowired
    public UserManagementService(final UserPersistencePort userPersistencePort,
                                 final UserRequestMapper userRequestMapper,
                                 final UserDtoMapper userDtoMapper) {
        this.userPersistencePort = userPersistencePort;
        this.userRequestMapper = userRequestMapper;
        this.userDtoMapper = userDtoMapper;
    }

    @Override
    public UserDto createNew(UserRequest request) {
        var UserRequest = userRequestMapper.toDomain(request);
        var userCreated = userPersistencePort.create(UserRequest);

        return userDtoMapper.toDto(userCreated);
    }

    @Override
    public UserDto getById(Long id) {
        var user = userPersistencePort.getById(id);
        return userDtoMapper.toDto(user);
    }

    @Override
    public List<UserDto> getAll() {
        var users = userPersistencePort.getAll();
        return users
                .stream()
                .map(userDtoMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        userPersistencePort.deleteById(id);
    }

    @Override
    public UserDto update(UserRequest request, Long id) {

        var user = userPersistencePort.getById(id);

        user.setRoles(userRequestMapper.toDomain(request).getRoles());

        var userUpdated = userPersistencePort.update(user);

        return userDtoMapper.toDto(userUpdated);
    }

}
