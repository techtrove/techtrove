package com.spin.techtrove.users.application.mapper;

import com.spin.techtrove.users.domain.model.Rol;
import com.spin.techtrove.users.domain.model.User;
import com.spin.techtrove.users.domain.model.dto.request.UserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserRequestMapper {

    @Mapping(source = "user", target = "user")
    @Mapping(source = "pass", target = "pass")
    @Mapping(source = "roles", target = "roles")
    User toDomain(UserRequest request);

}