package com.spin.techtrove.users.application.mapper;

import com.spin.techtrove.users.domain.model.User;
import com.spin.techtrove.users.domain.model.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring") 
public interface UserDtoMapper {

    @Mapping(source = "user", target = "user")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "roles", target = "roles")
    UserDto toDto(User domain);


}
