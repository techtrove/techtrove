package com.spin.techtrove.users.application.mapper;

import com.spin.techtrove.users.domain.model.Rol;
import com.spin.techtrove.users.domain.model.dto.RolDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring") 
public interface RolDtoMapper {

    //@Mapping(source = "id", target = "id")
    @Mapping(source = "description", target = "description")
    RolDto toDto(Rol domain);

}
