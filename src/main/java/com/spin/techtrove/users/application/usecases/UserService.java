package com.spin.techtrove.users.application.usecases;

import com.spin.techtrove.orders.domain.model.dto.OrderDto;
import com.spin.techtrove.orders.domain.model.dto.request.OrderRequest;
import com.spin.techtrove.users.domain.model.dto.UserDto;
import com.spin.techtrove.users.domain.model.dto.request.UserRequest;

import java.util.List;

public interface UserService {
    UserDto createNew(UserRequest request);
    UserDto getById(Long id);
    List<UserDto> getAll();
    void deleteById(Long id);
    UserDto update(UserRequest request, Long id);

}
