package com.spin.techtrove.users.domain.model;

import com.spin.techtrove.products.infraestructure.adapter.exception.InvalidProductException;
import com.spin.techtrove.users.infraestructure.adapter.exception.UserException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class User {

    private Long id;
    private String user;
    private String pass;
    private String name;
    private List<Rol> roles;

    public User(Long id, String user, String pass, String name, List<Rol> roles) {
        this.id = id;
        this.user = user;
        this.pass = pass;
        this.name = name;
        this.roles = roles;
    }

    public void validateCodeProduct(){
        if(this.user.isBlank() || this.pass.isBlank()){
            throw new UserException("Favor de verificar usuario y password");
        }
    }
}
