package com.spin.techtrove.users.domain.model;

import com.spin.techtrove.users.infraestructure.adapter.entity.UserE;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Rol {

    private String description;
    private UserE user;

}
