package com.spin.techtrove.users.domain.model.dto.request;

import com.spin.techtrove.users.domain.model.Rol;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserRequest {

    private String user;
    private String pass;
    private String name;
    private List<Rol> roles;

}
