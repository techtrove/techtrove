package com.spin.techtrove.users.domain.model.constant;

public class UserConstant {
    public static final String USER_NOT_FOUND_MESSAGE_ERROR = "El usuario no se encuentra con el id %s";
    public static final String USER_NOT_AUTH = "El usuario no puede ingresar con las credenciales.";
}
