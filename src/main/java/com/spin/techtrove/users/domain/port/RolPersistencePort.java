package com.spin.techtrove.users.domain.port;

import com.spin.techtrove.users.domain.model.Rol;

import java.util.List;

public interface RolPersistencePort {

    List<Rol> create(List<Rol> orderItems);

}
