package com.spin.techtrove.users.domain.port;

import com.spin.techtrove.users.domain.model.User;

import java.util.List;

public interface UserPersistencePort {

    User create(User order);
    User getById(Long id);
    List<User> getAll();
    void deleteById(Long id);
    User update(User user);

}
