package com.spin.techtrove.orders.domain.model;

import com.spin.techtrove.orders.infraestructure.adapter.entity.OrderE;
import com.spin.techtrove.orders.infraestructure.adapter.entity.OrderItemE;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderItem {

    private Long id;
    private String description;
    private int quantity;
    private BigDecimal price;
    private OrderE orderItem;

}
