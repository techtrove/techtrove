package com.spin.techtrove.orders.domain.model;

import com.spin.techtrove.orders.infraestructure.adapter.entity.OrderE;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Payment {

    private Long id;
    private String code;
    private OrderE order;

    public Payment() {}

    public Payment(Long id, String code) {
    }
}
