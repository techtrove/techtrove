package com.spin.techtrove.orders.domain.model.constant;

public class OrderConstant {
    public static final String ORDER_NOT_FOUND_MESSAGE_ERROR = "La orden no se encuentra con el id %s";
}
