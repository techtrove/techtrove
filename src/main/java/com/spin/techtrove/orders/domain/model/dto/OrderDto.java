package com.spin.techtrove.orders.domain.model.dto;

import com.spin.techtrove.orders.infraestructure.adapter.exception.InvalidOrderException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderDto {

    private Long id;
    private String numOrder;
    private BigDecimal total;
    private List<OrderItemDto> orderItems;
    private PaymentDto payment;

}
