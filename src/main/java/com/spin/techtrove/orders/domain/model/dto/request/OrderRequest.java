package com.spin.techtrove.orders.domain.model.dto.request;

import com.spin.techtrove.orders.domain.model.dto.OrderDto;
import com.spin.techtrove.orders.domain.model.dto.OrderItemDto;
import com.spin.techtrove.orders.domain.model.dto.PaymentDto;
import com.spin.techtrove.orders.infraestructure.adapter.exception.InvalidOrderException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderRequest {

    private Long id;
    private String numOrder;
    private List<OrderItemDto> orderItems;
    private PaymentDto payment;
    private BigDecimal total;

}
