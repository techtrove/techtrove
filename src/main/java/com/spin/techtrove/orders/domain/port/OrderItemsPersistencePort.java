package com.spin.techtrove.orders.domain.port;

import com.spin.techtrove.orders.domain.model.OrderItem;

import java.util.List;

public interface OrderItemsPersistencePort {

    List<OrderItem> create(List<OrderItem> orderItems);

}
