package com.spin.techtrove.orders.domain.port;

import com.spin.techtrove.orders.domain.model.Order;
import com.spin.techtrove.products.domain.model.Product;

import java.util.List;

public interface OrderPersistencePort {

    Order create(Order order);
    Order getById(Long id);
    List<Order> getAll();
    void deleteById(Long id);
    Order update(Order user);

}
