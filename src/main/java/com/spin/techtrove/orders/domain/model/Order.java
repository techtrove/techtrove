package com.spin.techtrove.orders.domain.model;

import com.spin.techtrove.orders.infraestructure.adapter.exception.InvalidOrderException;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class Order {

    private Long id;
    private String numOrder;
    private BigDecimal total;
    private List<OrderItem> orderItems;
    private Payment payment;

    public Order() {}

    public Order(Long id, String numOrder, BigDecimal total, List<OrderItem> orderItems, Payment payment) {
        this.id = id;
        this.numOrder = numOrder;
        this.total = total;
        this.orderItems = orderItems;
        this.payment = payment;
    }

    public void validateOrder(){
        Long negativeId = -1L;
        if(this.id == null || negativeId.compareTo(this.id) < 0){
            throw new InvalidOrderException("El ID de la orden es invalido.");
        }

        if(this.orderItems.isEmpty() || this.payment == null){
            throw new InvalidOrderException("Faltan valores necesarios en la orden.");
        }
    }


    @Override
    public boolean equals(Object obj){

        if(!(obj instanceof Order)) return false;

        Order order = (Order) obj;
        if (this.id==null || this.numOrder==null)
            return false;

        return this.id.equals(order.id);
    }

}
