package com.spin.techtrove.orders.domain.model.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PaymentDto {

    private Long id;
    private String code;

}
