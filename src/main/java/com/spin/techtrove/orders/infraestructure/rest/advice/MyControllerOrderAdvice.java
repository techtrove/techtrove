package com.spin.techtrove.orders.infraestructure.rest.advice;

import com.spin.techtrove.orders.infraestructure.adapter.exception.OrderException;
import com.spin.techtrove.products.infraestructure.adapter.exception.CategoryException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class MyControllerOrderAdvice {

    @ExceptionHandler(OrderException.class)
    public ResponseEntity<String> handleEmptyInput(CategoryException emptyInputException){
        return new ResponseEntity<String>(emptyInputException.getErrorMessage(), emptyInputException.getErrorCode());
    }

}
