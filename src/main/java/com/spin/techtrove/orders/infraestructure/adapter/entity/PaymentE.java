package com.spin.techtrove.orders.infraestructure.adapter.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "payment", indexes = {
        @Index(name = "idx_id", columnList = "id")
})
@Getter
@Setter
public class PaymentE implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "orderE_id", nullable = false)
    private OrderE order;

}
