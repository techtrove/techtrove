package com.spin.techtrove.orders.infraestructure.adapter;

import com.spin.techtrove.orders.domain.model.Order;
import com.spin.techtrove.orders.domain.port.OrderPersistencePort;
import com.spin.techtrove.orders.infraestructure.adapter.entity.OrderItemE;
import com.spin.techtrove.orders.infraestructure.adapter.mapper.OrderDboMapper;
import com.spin.techtrove.orders.infraestructure.adapter.mapper.OrderItemDboMapper;
import com.spin.techtrove.orders.infraestructure.adapter.mapper.OrderPaymentDboMapper;
import com.spin.techtrove.orders.infraestructure.adapter.repository.OrderItemRepository;
import com.spin.techtrove.orders.infraestructure.adapter.repository.OrderRepository;
import com.spin.techtrove.products.domain.model.constant.CategoryConstant;
import com.spin.techtrove.products.infraestructure.adapter.exception.CategoryException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderSpringJpaAdapter implements OrderPersistencePort {
    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;
    private final OrderDboMapper orderDboMapper;
    private final OrderItemDboMapper orderItemDboMapper;
    private final OrderPaymentDboMapper orderPaymentDboMapper;

    public OrderSpringJpaAdapter(OrderRepository orderRepository, OrderDboMapper orderDboMapper, OrderPaymentDboMapper orderPaymentDboMapper, OrderItemDboMapper orderItemDboMapper, OrderItemRepository orderItemRepository ) {
        this.orderDboMapper = orderDboMapper;
        this.orderRepository = orderRepository;
        this.orderPaymentDboMapper = orderPaymentDboMapper;
        this.orderItemDboMapper = orderItemDboMapper;
        this.orderItemRepository = orderItemRepository;
    }

    @Override
    public Order create(Order request) {

        var orderToSave = orderDboMapper.toDbo(request);
        var paymentOrder = orderPaymentDboMapper.toDbo(request.getPayment());

        orderToSave.setPayment(paymentOrder);

        paymentOrder.setOrder(orderToSave);

        var orderSaved = orderRepository.save(orderToSave);

        Set<OrderItemE> orderItems = request.getOrderItems().stream()
                .map(item -> {
                    var order = orderItemDboMapper.toDbo(item);
                    order.setOrderItem(orderSaved);
                    return order;
                })
                .collect(Collectors.toSet());

        orderItemRepository.saveAll(orderItems);

        var orderReturn = orderDboMapper.toDomain(orderSaved);
        orderReturn.setOrderItems(request.getOrderItems());

        return orderReturn;

    }

    @Override
    public Order getById(Long id) {
        var optionalOrder = orderRepository.findById(id);

        if (optionalOrder.isEmpty()){
            throw new CategoryException(HttpStatus.NOT_FOUND, String.format(CategoryConstant.CAT_NOT_FOUND_MESSAGE_ERROR, id));
        }

        return orderDboMapper.toDomain(optionalOrder.get());
    }

    @Override
    public List<Order> getAll() {

        return orderRepository.findAll().stream()
                .map(orderDboMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        orderRepository.deleteById(id);
    }

    @Override
    public Order update(Order category) {

        var categoryToUpdate = orderDboMapper.toDbo(category);
        var categoryUpdated = orderRepository.save(categoryToUpdate);

        return orderDboMapper.toDomain(categoryUpdated);

    }

}
