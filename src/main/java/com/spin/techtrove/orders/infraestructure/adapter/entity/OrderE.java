package com.spin.techtrove.orders.infraestructure.adapter.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.spin.techtrove.orders.domain.model.OrderItem;
import com.spin.techtrove.users.infraestructure.adapter.entity.RolE;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "orders", indexes = {
        @Index(name = "idx_id", columnList = "id")
})
@Getter
@Setter
public class OrderE implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String numOrder;
    private BigDecimal total;

    @OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "order")
    private PaymentE payment;

    @OneToMany(fetch = FetchType.LAZY,
            cascade =  CascadeType.MERGE,
            mappedBy = "orderItem")
    private List<OrderItemE> orderItems;

}
