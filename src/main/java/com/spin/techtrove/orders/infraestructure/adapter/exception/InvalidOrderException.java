package com.spin.techtrove.orders.infraestructure.adapter.exception;

public class InvalidOrderException extends RuntimeException {
    public InvalidOrderException() {
        super();
    }

    public InvalidOrderException(String message) {
        super(message);
    }
}
