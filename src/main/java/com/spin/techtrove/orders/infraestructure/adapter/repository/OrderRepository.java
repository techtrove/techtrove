package com.spin.techtrove.orders.infraestructure.adapter.repository;

import com.spin.techtrove.orders.infraestructure.adapter.entity.OrderE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<OrderE, Long> {

    List<OrderE> findByIdIn(List<Long> catsIds);

}