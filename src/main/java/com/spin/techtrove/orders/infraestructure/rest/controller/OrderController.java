package com.spin.techtrove.orders.infraestructure.rest.controller;

import com.spin.techtrove.orders.application.usecases.OrderService;
import com.spin.techtrove.orders.domain.model.dto.OrderDto;
import com.spin.techtrove.orders.domain.model.dto.request.OrderRequest;
import com.spin.techtrove.products.domain.model.dto.request.CategoryRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/orders")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/obtener/{id}")
    public OrderDto getById(@PathVariable Long id){
        return orderService.getById(id);
    }

    @GetMapping
    public List<OrderDto> getAll() {
        return orderService.getAll();
    }

    @PostMapping("/crear")
    public OrderDto create(@RequestBody OrderRequest catRequest){
        return orderService.createNew(catRequest);
    }

    @PutMapping("/actualizar/{id}")
    public OrderDto edit(@RequestBody OrderRequest catRequest,
                               @PathVariable Long id){
        return orderService.update(catRequest, id);
    }

    @DeleteMapping("/borrar/{id}")
    public void deleteById(@PathVariable Long id){
        orderService.deleteById(id);
    }

}
