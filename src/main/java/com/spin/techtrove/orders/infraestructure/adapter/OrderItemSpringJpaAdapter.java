package com.spin.techtrove.orders.infraestructure.adapter;

import com.spin.techtrove.orders.domain.model.Order;
import com.spin.techtrove.orders.domain.model.OrderItem;
import com.spin.techtrove.orders.domain.port.OrderItemsPersistencePort;
import com.spin.techtrove.orders.domain.port.OrderPersistencePort;
import com.spin.techtrove.orders.infraestructure.adapter.entity.OrderE;
import com.spin.techtrove.orders.infraestructure.adapter.entity.OrderItemE;
import com.spin.techtrove.orders.infraestructure.adapter.mapper.OrderDboMapper;
import com.spin.techtrove.orders.infraestructure.adapter.mapper.OrderItemDboMapper;
import com.spin.techtrove.orders.infraestructure.adapter.repository.OrderItemRepository;
import com.spin.techtrove.orders.infraestructure.adapter.repository.OrderRepository;
import com.spin.techtrove.products.domain.model.constant.CategoryConstant;
import com.spin.techtrove.products.infraestructure.adapter.exception.CategoryException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderItemSpringJpaAdapter implements OrderItemsPersistencePort {
    private final OrderItemRepository orderItemRepository;
    private final OrderItemDboMapper orderItemDboMapper;
    public OrderItemSpringJpaAdapter(OrderItemRepository orderItemRepository, OrderItemDboMapper orderItemDboMapper) {
        this.orderItemDboMapper = orderItemDboMapper;
        this.orderItemRepository = orderItemRepository;
    }

    @Override
    public List<OrderItem> create(List<OrderItem> request) {

        List<OrderItemE> orderItems = request.stream()
                .map(orderItemDboMapper::toDbo)
                .collect(Collectors.toList());

        orderItemRepository.saveAll(orderItems);

        return request;

    }

}
