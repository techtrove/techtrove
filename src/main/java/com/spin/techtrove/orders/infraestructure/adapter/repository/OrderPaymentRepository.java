package com.spin.techtrove.orders.infraestructure.adapter.repository;

import com.spin.techtrove.orders.infraestructure.adapter.entity.PaymentE;
import org.springframework.data.jpa.repository.JpaRepository;


public interface OrderPaymentRepository extends JpaRepository<PaymentE, Long> {
}
