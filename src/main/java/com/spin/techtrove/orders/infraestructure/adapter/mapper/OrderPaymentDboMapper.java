package com.spin.techtrove.orders.infraestructure.adapter.mapper;

import com.spin.techtrove.orders.domain.model.OrderItem;
import com.spin.techtrove.orders.domain.model.Payment;
import com.spin.techtrove.orders.infraestructure.adapter.entity.OrderItemE;
import com.spin.techtrove.orders.infraestructure.adapter.entity.PaymentE;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface OrderPaymentDboMapper {

    //@Mapping(source = "id", target = "id")
    @Mapping(source = "code", target = "code")
    @Mapping(source = "order", target = "order")
    PaymentE toDbo(Payment domain);

    @InheritInverseConfiguration
    Payment toDomain(PaymentE entity);
}