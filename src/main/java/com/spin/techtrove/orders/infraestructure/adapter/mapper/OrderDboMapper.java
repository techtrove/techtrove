package com.spin.techtrove.orders.infraestructure.adapter.mapper;

import com.spin.techtrove.orders.domain.model.Order;
import com.spin.techtrove.orders.infraestructure.adapter.entity.OrderE;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface OrderDboMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "numOrder", target = "numOrder")
    @Mapping(source = "orderItems", target = "orderItems")
    @Mapping(source = "payment", target = "payment")
    @Mapping(source = "total", target = "total")
    OrderE toDbo(Order domain);

    @InheritInverseConfiguration
    Order toDomain(OrderE entity);
}