package com.spin.techtrove.orders.infraestructure.adapter.mapper;

import com.spin.techtrove.orders.domain.model.OrderItem;
import com.spin.techtrove.orders.infraestructure.adapter.entity.OrderItemE;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface OrderItemDboMapper {

    //@Mapping(source = "id", target = "id")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "price", target = "price")
    @Mapping(source = "quantity", target = "quantity")
    @Mapping(source = "orderItem", target = "orderItem")
    OrderItemE toDbo(OrderItem domain);

    @InheritInverseConfiguration
    OrderItem toDomain(OrderItemE entity);
}