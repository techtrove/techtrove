package com.spin.techtrove.orders.infraestructure.adapter.repository;

import com.spin.techtrove.orders.infraestructure.adapter.entity.OrderItemE;
import org.springframework.data.jpa.repository.JpaRepository;


public interface OrderItemRepository extends JpaRepository<OrderItemE, Long> {
}
