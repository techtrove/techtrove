package com.spin.techtrove.orders.application.mapper;

import com.spin.techtrove.orders.domain.model.Order;
import com.spin.techtrove.orders.domain.model.dto.OrderDto;
import com.spin.techtrove.products.domain.model.Category;
import com.spin.techtrove.products.domain.model.dto.CategoryDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring") 
public interface OrderDtoMapper {


    @Mapping(source = "id", target = "id")
    @Mapping(source = "total", target = "total")
    @Mapping(source = "orderItems", target = "orderItems")
    @Mapping(source = "payment", target = "payment")
    OrderDto toDto(Order domain);

}
