package com.spin.techtrove.orders.application.usecases;

import com.spin.techtrove.orders.domain.model.dto.OrderDto;
import com.spin.techtrove.orders.domain.model.dto.request.OrderRequest;

import java.util.List;

public interface OrderService {
    OrderDto createNew(OrderRequest request);
    OrderDto getById(Long id);
    List<OrderDto> getAll();
    void deleteById(Long id);
    OrderDto update(OrderRequest request, Long id);
    //void assignPayment(Long id, Long catsIds);

}
