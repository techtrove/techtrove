package com.spin.techtrove.orders.application.service;

import com.spin.techtrove.orders.application.mapper.OrderDtoMapper;
import com.spin.techtrove.orders.application.mapper.OrderRequestMapper;
import com.spin.techtrove.orders.application.usecases.OrderService;
import com.spin.techtrove.orders.domain.model.dto.OrderDto;
import com.spin.techtrove.orders.domain.model.dto.request.OrderRequest;
import com.spin.techtrove.orders.domain.port.OrderItemsPersistencePort;
import com.spin.techtrove.orders.domain.port.OrderPersistencePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderManagementService implements OrderService {

    private final OrderPersistencePort orderPersistencePort;
    private final OrderRequestMapper orderRequestMapper;
    private final OrderDtoMapper orderDtoMapper;

    @Autowired
    public OrderManagementService(final OrderPersistencePort orderPersistencePort,
                                  final OrderItemsPersistencePort orderItemPersistencePort,
                                  final OrderRequestMapper orderRequestMapper,
                                  final OrderDtoMapper orderDtoMapper) {
        this.orderPersistencePort = orderPersistencePort;
        this.orderRequestMapper = orderRequestMapper;
        this.orderDtoMapper = orderDtoMapper;
    }

    @Override
    public OrderDto createNew(OrderRequest request) {
        var orderRequest = orderRequestMapper.toDomain(request);
        var orderCreated = orderPersistencePort.create(orderRequest);

        return orderDtoMapper.toDto(orderCreated);
    }

    @Override
    public OrderDto getById(Long id) {
        var order = orderPersistencePort.getById(id);
        return orderDtoMapper.toDto(order);
    }

    @Override
    public List<OrderDto> getAll() {
        var orders = orderPersistencePort.getAll();
        return orders
                .stream()
                .map(orderDtoMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        orderPersistencePort.deleteById(id);
    }

    @Override
    public OrderDto update(OrderRequest request, Long id) {

        var order = orderPersistencePort.getById(id);

        order.setPayment(orderRequestMapper.toDomain(request).getPayment());

        var orderUpdated = orderPersistencePort.update(order);

        return orderDtoMapper.toDto(orderUpdated);
    }

}
