package com.spin.techtrove.orders.application.mapper;

import com.spin.techtrove.orders.domain.model.OrderItem;
import com.spin.techtrove.orders.domain.model.dto.OrderItemDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring") 
public interface OrderItemDtoMapper {


    @Mapping(source = "id", target = "id")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "price", target = "price")
    @Mapping(source = "quantity", target = "quantity")
    OrderItemDto toDto(OrderItem domain);

}
