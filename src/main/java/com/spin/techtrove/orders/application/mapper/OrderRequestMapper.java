package com.spin.techtrove.orders.application.mapper;

import com.spin.techtrove.orders.domain.model.Order;
import com.spin.techtrove.orders.domain.model.OrderItem;
import com.spin.techtrove.orders.domain.model.Payment;
import com.spin.techtrove.orders.domain.model.dto.request.OrderRequest;
import com.spin.techtrove.products.domain.model.Category;
import com.spin.techtrove.products.domain.model.dto.request.CategoryRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring") 
public interface OrderRequestMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(source = "numOrder", target = "numOrder")
    @Mapping(source = "orderItems", target = "orderItems")
    @Mapping(source = "payment", target = "payment")
    Order toDomain(OrderRequest request);

}
