package com.spin.techtrove.products.application.service;

import com.spin.techtrove.products.application.mapper.CategoryDtoMapper;
import com.spin.techtrove.products.application.mapper.CategoryRequestMapper;
import com.spin.techtrove.products.application.usecases.CategoryService;
import com.spin.techtrove.products.domain.model.constant.CategoryConstant;
import com.spin.techtrove.products.domain.model.dto.CategoryDto;
import com.spin.techtrove.products.domain.model.dto.request.CategoryRequest;
import com.spin.techtrove.products.domain.port.CategoryPersistencePort;
import com.spin.techtrove.products.infraestructure.adapter.exception.ProductException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryManagementService implements CategoryService {

    private final CategoryPersistencePort categoryPersistencePort;
    private final CategoryRequestMapper categoryRequestMapper;
    private final CategoryDtoMapper categoryDtoMapper;

    @Autowired
    public CategoryManagementService(final CategoryPersistencePort categoryPersistencePort,
                                     final CategoryRequestMapper categoryRequestMapper,
                                     final CategoryDtoMapper categoryDtoMapper) {
        this.categoryPersistencePort = categoryPersistencePort;
        this.categoryRequestMapper = categoryRequestMapper;
        this.categoryDtoMapper = categoryDtoMapper;
    }

    @Override
    public CategoryDto createNew(CategoryRequest request) {
        var categoryToCreate = categoryRequestMapper.toDomain(request);

        var categoryCreated = categoryPersistencePort.create(categoryToCreate);

        return categoryDtoMapper.toDto(categoryCreated);
    }

    @Override
    public CategoryDto getById(Long id) {
        var category = categoryPersistencePort.getById(id);

        return categoryDtoMapper.toDto(category);
    }

    @Override
    public List<CategoryDto> getAll() {
        var categories = categoryPersistencePort.getAll();
        return categories
                .stream()
                .map(categoryDtoMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        var category = categoryPersistencePort.getById(id);

        if(category.getProducts()==null){
            throw new ProductException(HttpStatus.BAD_REQUEST,
                    String.format(CategoryConstant.CURRENT_CATEGORY_NOT_ALLOW_TO_DELETE, category.getId()));
        }
        categoryPersistencePort.deleteById(id);
    }

    @Override
    public CategoryDto update(CategoryRequest request, Long id) {
        var categoryToUpdate = categoryRequestMapper.toDomain(request);

        categoryToUpdate.setName(request.getName());
        categoryToUpdate.setDescription(request.getDescription());

        var categoryUpdated = categoryPersistencePort.update(categoryToUpdate);

        return categoryDtoMapper.toDto(categoryUpdated);

    }

}
