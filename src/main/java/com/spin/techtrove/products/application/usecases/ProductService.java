package com.spin.techtrove.products.application.usecases;

import com.spin.techtrove.products.domain.model.dto.ProductDto;
import com.spin.techtrove.products.domain.model.dto.request.ProductRequest;

import java.util.List;

public interface ProductService {
    ProductDto createNew(ProductRequest request);
    ProductDto getById(Long id);
    List<ProductDto> getAll();
    void deleteById(Long id);
    ProductDto update(ProductRequest request, Long id);

    void assignCategory(Long id, Long catId);

}
