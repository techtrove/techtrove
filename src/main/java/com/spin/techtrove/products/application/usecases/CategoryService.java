package com.spin.techtrove.products.application.usecases;

import com.spin.techtrove.products.domain.model.dto.CategoryDto;
import com.spin.techtrove.products.domain.model.dto.request.CategoryRequest;

import java.util.List;

public interface CategoryService {

    CategoryDto createNew(CategoryRequest request);
    CategoryDto getById(Long id);
    List<CategoryDto> getAll();
    void deleteById(Long id);
    CategoryDto update(CategoryRequest request, Long id);

}
