package com.spin.techtrove.products.application.mapper;

import com.spin.techtrove.products.domain.model.Product;
import com.spin.techtrove.products.domain.model.dto.ProductDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.math.BigDecimal;

@Mapper(componentModel = "spring") 
public interface ProductDtoMapper {
    
    @Mapping(source = "code", target = "code")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "price", target = "price")
    @Mapping(source = "quantity", target = "quantity")
    @Mapping(source = "categories", target = "categories")
    ProductDto toDto(Product domain);

}
