package com.spin.techtrove.products.application.mapper;

import com.spin.techtrove.products.domain.model.Category;
import com.spin.techtrove.products.domain.model.dto.CategoryDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring") 
public interface CategoryDtoMapper {


    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "description", target = "description")
    CategoryDto toDto(Category domain);

}
