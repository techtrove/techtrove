package com.spin.techtrove.products.application.mapper;

import com.spin.techtrove.products.domain.model.Product;
import com.spin.techtrove.products.domain.model.dto.request.ProductRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.math.BigDecimal;

@Mapper(componentModel = "spring") 
public interface ProductRequestMapper {

    @Mapping(source = "name", target = "name")
    @Mapping(source = "code", target = "code")
    @Mapping(source = "price", target = "price")
    @Mapping(source = "quantity", target = "quantity")
    Product toDomain(ProductRequest request);
}
