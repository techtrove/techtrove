package com.spin.techtrove.products.application.mapper;

import com.spin.techtrove.products.domain.model.Category;
import com.spin.techtrove.products.domain.model.dto.request.CategoryRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring") 
public interface CategoryRequestMapper {


    @Mapping(source = "name", target = "name")
    @Mapping(source = "description", target = "description")
    Category toDomain(CategoryRequest request);

}
