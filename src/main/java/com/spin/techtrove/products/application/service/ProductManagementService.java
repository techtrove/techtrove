package com.spin.techtrove.products.application.service;

import com.spin.techtrove.products.application.mapper.ProductDtoMapper;
import com.spin.techtrove.products.application.mapper.ProductRequestMapper;
import com.spin.techtrove.products.application.usecases.ProductService;
import com.spin.techtrove.products.domain.model.constant.ProductConstant;
import com.spin.techtrove.products.domain.model.dto.ProductDto;
import com.spin.techtrove.products.domain.model.dto.request.ProductRequest;
import com.spin.techtrove.products.domain.port.CategoryPersistencePort;
import com.spin.techtrove.products.domain.port.ProductPersistencePort;
import com.spin.techtrove.products.infraestructure.adapter.exception.ProductException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductManagementService implements ProductService {

    private final ProductPersistencePort productPersistencePort;
    private final CategoryPersistencePort categoryPersistencePort;
    private final ProductRequestMapper productRequestMapper;

    private final ProductDtoMapper productDtoMapper;

    @Autowired
    public ProductManagementService(final ProductPersistencePort productPersistencePort,
                                    final CategoryPersistencePort categoryPersistencePort,
                                    final ProductRequestMapper productRequestMapper,
                                    final ProductDtoMapper productDtoMapper) {
        this.productPersistencePort = productPersistencePort;
        this.categoryPersistencePort = categoryPersistencePort;
        this.productRequestMapper = productRequestMapper;
        this.productDtoMapper = productDtoMapper;
    }

    @Override
    public ProductDto createNew(ProductRequest request) {
        var productRequest = productRequestMapper.toDomain(request);
        var productCreated = productPersistencePort.create(productRequest);
        return productDtoMapper.toDto(productCreated);
    }

    @Override
    public ProductDto getById(Long id) {
        var product = productPersistencePort.getById(id);
        return productDtoMapper.toDto(product);
    }

    @Override
    public List<ProductDto> getAll() {
        var products = productPersistencePort.getAll();
        return products
                .stream()
                .map(productDtoMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        productPersistencePort.deleteById(id);
    }

    @Override
    public ProductDto update(ProductRequest request, Long id) {

        var product = productPersistencePort.getById(id);

        product.setName(request.getName());

        var productUpdated = productPersistencePort.update(product);

        return productDtoMapper.toDto(productUpdated);
    }

    @Override
    public void assignCategory(Long id, Long catId) {

        var product = productPersistencePort.getById(id);

        var category = categoryPersistencePort.getById(catId);

        if(category!=null){
            product.setCategory(category);
            productPersistencePort.update(product);
        }else{
            throw new ProductException(HttpStatus.BAD_REQUEST,
                    String.format(ProductConstant.CURRENT_PRODUCT_NOT_ALLOW_TO_ASSIGN, catId));
        }
    }

}
