package com.spin.techtrove.products.domain.port;

import com.spin.techtrove.products.domain.model.Product;

import java.util.List;

public interface ProductPersistencePort {

    Product create(Product user);
    Product getById(Long id);
    List<Product> getAll();
    void deleteById(Long id);
    Product update(Product user);

}
