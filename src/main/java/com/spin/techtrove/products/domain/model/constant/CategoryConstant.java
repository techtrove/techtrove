package com.spin.techtrove.products.domain.model.constant;

public class CategoryConstant {
    public static final String CAT_NOT_FOUND_MESSAGE_ERROR = "No se encontro una categoria con el id %s";
    public static final String CURRENT_CATEGORY_NOT_ALLOW_TO_DELETE = "La categoria %s no se puede eliminar porque tiene productos asignados";
}
