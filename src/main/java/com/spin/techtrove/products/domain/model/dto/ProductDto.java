package com.spin.techtrove.products.domain.model.dto;

import com.spin.techtrove.products.domain.model.Category;
import com.spin.techtrove.users.domain.model.dto.RolDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.mapstruct.Mapping;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductDto {

    private Long id;
    private String name;
    private String code;
    private int quantity;
    private BigDecimal price;
    private List<CategoryDto> categories;

}
