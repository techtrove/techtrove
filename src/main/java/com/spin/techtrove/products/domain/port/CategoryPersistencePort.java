package com.spin.techtrove.products.domain.port;

import com.spin.techtrove.products.domain.model.Category;

import java.util.List;

public interface CategoryPersistencePort {

    Category create(Category request);
    Category getById(Long id);
    List<Category> getAll();
    void deleteById(Long id);
    Category update(Category request);
    List<Category> getByIds(List<Long> catsIds);

}
