package com.spin.techtrove.products.domain.model;

import com.spin.techtrove.products.infraestructure.adapter.entity.ProductEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Category {

    private Long id;
    private String name;
    private String description;

    private ProductEntity products;

}
