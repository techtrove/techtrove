package com.spin.techtrove.products.domain.model;


import com.spin.techtrove.products.infraestructure.adapter.exception.InvalidProductException;
import com.spin.techtrove.products.infraestructure.adapter.exception.ProductHasNotCatException;
import com.spin.techtrove.products.infraestructure.adapter.exception.ProductPriceException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
public class Product {

    private Long id;
    private String code;
    private String name;
    private BigDecimal price;
    private int quantity;
    private Category category;
    private List<Category> categories;

    public void validateCodeProduct(){
        if(this.code.isBlank()){
            throw new InvalidProductException("El Codigo del producto es invalido.");
        }
    }

    public void validateProductCategory(){
        if(this.getCategory() == null || this.getCategories().isEmpty()){
            throw new ProductHasNotCatException("El producto requiere una categoria.");
        }
    }
    public void validatePrice(){

        if(this.price.compareTo(BigDecimal.ZERO) <= 0){
            throw new ProductPriceException("El precio del producto no puede ser menor o igual a 0.");
        }
    }

    public Product() {
        this.code = UUID.randomUUID().toString();
    }

    public Product(Long id, String name, BigDecimal price, int quantity) {
        this.id = id;
        this.code = UUID.randomUUID().toString();
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }
}
