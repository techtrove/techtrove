package com.spin.techtrove.products.domain.model.dto.request;

import com.spin.techtrove.products.domain.model.Category;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductRequest {

    private Long id;
    private String name;
    private String code;
    private String price;
    private int quantity;
    private List<Category> categories;

}
