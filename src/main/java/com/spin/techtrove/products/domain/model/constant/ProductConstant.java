package com.spin.techtrove.products.domain.model.constant;

public class ProductConstant {
    public static final String CATEGORY_NOT_FOUND_MESSAGE_ERROR = "No se encontro una categoria con el id %s";
    public static final String CURRENT_PRODUCT_NOT_ALLOW_TO_ASSIGN = "No existe la categoria para asignar al producto.";
    public static final String PRODUCT_NOT_FOUND_MESSAGE_ERROR = "El producto no se encuentra con el id %s";
}
