package com.spin.techtrove.products.infraestructure.adapter.exception;

public class InvalidProductException extends RuntimeException {
    public InvalidProductException() {
        super();
    }

    public InvalidProductException(String message) {
        super(message);
    }
}
