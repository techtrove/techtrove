package com.spin.techtrove.products.infraestructure.adapter.mapper;

import com.spin.techtrove.products.domain.model.Product;
import com.spin.techtrove.products.infraestructure.adapter.entity.ProductEntity;
import com.spin.techtrove.users.infraestructure.adapter.entity.RolE;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.math.BigDecimal;
import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductDboMapper {

    @Mapping(source = "code", target = "code")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "price", target = "price")
    @Mapping(source = "quantity", target = "quantity")
    @Mapping(source = "categories", target = "categories")
    ProductEntity toDbo(Product domain);

    @InheritInverseConfiguration
    Product toDomain(ProductEntity entity);
}