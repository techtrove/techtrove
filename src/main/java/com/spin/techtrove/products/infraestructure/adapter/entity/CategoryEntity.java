package com.spin.techtrove.products.infraestructure.adapter.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.spin.techtrove.users.infraestructure.adapter.entity.UserE;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "categories")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CategoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "productEntity_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ProductEntity products;

    /*@ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "categoryEntities")
    @JsonIgnore
    private Set<ProductEntity> userEntities = new HashSet<>();

    public CategoryEntity(Long id) {
        this.id = id;
    }*/

}
