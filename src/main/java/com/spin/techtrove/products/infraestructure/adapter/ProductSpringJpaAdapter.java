package com.spin.techtrove.products.infraestructure.adapter;

import com.spin.techtrove.products.domain.model.Product;
import com.spin.techtrove.products.domain.model.constant.ProductConstant;
import com.spin.techtrove.products.domain.port.ProductPersistencePort;
import com.spin.techtrove.products.infraestructure.adapter.entity.CategoryEntity;
import com.spin.techtrove.products.infraestructure.adapter.exception.ProductException;
import com.spin.techtrove.products.infraestructure.adapter.mapper.CategoryDboMapper;
import com.spin.techtrove.products.infraestructure.adapter.mapper.ProductDboMapper;
import com.spin.techtrove.products.infraestructure.adapter.repository.CategoryRepository;
import com.spin.techtrove.products.infraestructure.adapter.repository.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
@Transactional
public class ProductSpringJpaAdapter implements ProductPersistencePort {
    private final ProductRepository productRepository;
    private final ProductDboMapper productDboMapper;

    private final CategoryRepository rolRepository;
    private final CategoryDboMapper categoryDboMapper;

    public ProductSpringJpaAdapter(ProductRepository productRepository, ProductDboMapper productDboMapper, CategoryDboMapper categoryDboMapper,CategoryRepository rolRepository) {
        this.productRepository = productRepository;
        this.productDboMapper = productDboMapper;
        this.categoryDboMapper = categoryDboMapper;
        this.rolRepository = rolRepository;
    }

    @Override
    public Product create(Product product) {

        var userToSave = productDboMapper.toDbo(product);
        var userSaved = productRepository.save(userToSave);

        Set<CategoryEntity> orderItems = product.getCategories().stream()
                .map(item -> {
                    var category = categoryDboMapper.toDbo(item);
                    category.setProducts(userSaved);
                    return category;
                })
                .collect(Collectors.toSet());

        rolRepository.saveAll(orderItems);

        return productDboMapper.toDomain(userSaved);
    }

    @Override
    public Product getById(Long id) {

        var optionalUser = productRepository.findById(id);

        if (optionalUser.isEmpty()){
            throw new ProductException(HttpStatus.NOT_FOUND,
                    String.format(ProductConstant.PRODUCT_NOT_FOUND_MESSAGE_ERROR, id));
        }

        return productDboMapper.toDomain(optionalUser.get());
    }

    @Override
    public List<Product> getAll() {
        return productRepository.findAll()
                .stream()
                .map(productDboMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Product update(Product product) {

        var productToUpdate = productDboMapper.toDbo(product);
        var productUpdated = productRepository.save(productToUpdate);

        return productDboMapper.toDomain(productUpdated);
    }

}
