package com.spin.techtrove.products.infraestructure.rest.advice;

import com.spin.techtrove.products.infraestructure.adapter.exception.CategoryException;
import com.spin.techtrove.products.infraestructure.adapter.exception.ProductException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class MyControllerAdvice {

    @ExceptionHandler(CategoryException.class)
    public ResponseEntity<String> handleEmptyInput(CategoryException emptyInputException){
        return new ResponseEntity<String>(emptyInputException.getErrorMessage(), emptyInputException.getErrorCode());
    }

    @ExceptionHandler(ProductException.class)
    public ResponseEntity<String> handleEmptyInput(ProductException emptyInputException){
        return new ResponseEntity<String>(emptyInputException.getErrorMessage(), emptyInputException.getErrorCode());
    }

}
