package com.spin.techtrove.products.infraestructure.adapter.exception;

public class ProductHasNotCatException extends RuntimeException {
    public ProductHasNotCatException() {
        super();
    }

    public ProductHasNotCatException(String message) {
        super(message);
    }
}
