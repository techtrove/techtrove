package com.spin.techtrove.products.infraestructure.adapter.mapper;

import com.spin.techtrove.products.domain.model.Category;
import com.spin.techtrove.products.infraestructure.adapter.entity.CategoryEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CategoryDboMapper {
    
    @Mapping(source = "name", target = "name")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "products", target = "products")
    CategoryEntity toDbo(Category domain);

    @InheritInverseConfiguration
    Category toDomain(CategoryEntity entity);
}