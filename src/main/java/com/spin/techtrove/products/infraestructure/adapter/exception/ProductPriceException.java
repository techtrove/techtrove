package com.spin.techtrove.products.infraestructure.adapter.exception;

public class ProductPriceException extends RuntimeException {
    public ProductPriceException() {
        super();
    }

    public ProductPriceException(String message) {
        super(message);
    }
}
