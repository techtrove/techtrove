package com.spin.techtrove.products.infraestructure.rest.controller;

import com.spin.techtrove.products.application.usecases.ProductService;
import com.spin.techtrove.products.domain.model.dto.ProductDto;
import com.spin.techtrove.products.domain.model.dto.request.ProductRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/obtener/{id}")
    public ProductDto getById(@PathVariable long id){
        return productService.getById(id);
    }

    @GetMapping
    public List<ProductDto> getAll() {
        return productService.getAll();
    }

    @PostMapping("/crear")
    public ProductDto create(@RequestBody ProductRequest productRequest){
        return productService.createNew(productRequest);
    }

    @PutMapping("/actualizar/{id}")
    public ProductDto userEdit(@RequestBody ProductRequest productRequest,
                               @PathVariable Long id){
        return productService.update(productRequest, id);
    }

    @DeleteMapping("/borrar/{id}")
    public void deleteUserById(@PathVariable Long id){
        productService.deleteById(id);
    }

    @PostMapping("/{id}/category")
    public void assignCategory(@PathVariable Long id , @RequestParam Long catId){
        //productService.assignCategory(id, catId);
    }

}
