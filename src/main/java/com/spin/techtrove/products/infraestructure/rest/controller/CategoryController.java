package com.spin.techtrove.products.infraestructure.rest.controller;

import com.spin.techtrove.products.application.usecases.CategoryService;
import com.spin.techtrove.products.domain.model.dto.CategoryDto;
import com.spin.techtrove.products.domain.model.dto.request.CategoryRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/categories")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/{id}")
    public CategoryDto getById(@PathVariable Long id){
        return categoryService.getById(id);
    }

    @GetMapping
    public List<CategoryDto> getAll() {
        return categoryService.getAll();
    }

    @PostMapping()
    public CategoryDto create(@RequestBody CategoryRequest catRequest){
        return categoryService.createNew(catRequest);
    }

    @PutMapping("/{id}")
    public CategoryDto edit(@RequestBody CategoryRequest catRequest,
                               @PathVariable Long id){
        return categoryService.update(catRequest, id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id){
        categoryService.deleteById(id);
    }

}
