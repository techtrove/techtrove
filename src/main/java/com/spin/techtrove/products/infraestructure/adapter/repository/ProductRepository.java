package com.spin.techtrove.products.infraestructure.adapter.repository;

import com.spin.techtrove.products.infraestructure.adapter.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

}