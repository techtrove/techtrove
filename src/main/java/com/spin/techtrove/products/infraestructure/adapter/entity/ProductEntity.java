package com.spin.techtrove.products.infraestructure.adapter.entity;


import com.spin.techtrove.users.infraestructure.adapter.entity.RolE;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "products", indexes = {
        @Index(name = "idx_id", columnList = "id")
})
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String code;
    private BigDecimal price;
    private int quantity;

    @OneToMany(fetch = FetchType.LAZY,
            cascade =  CascadeType.MERGE,
            mappedBy = "products")
    private List<CategoryEntity> categories;

}
