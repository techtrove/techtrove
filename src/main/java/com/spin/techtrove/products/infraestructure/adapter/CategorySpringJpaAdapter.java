package com.spin.techtrove.products.infraestructure.adapter;

import com.spin.techtrove.products.domain.model.Category;
import com.spin.techtrove.products.domain.model.constant.CategoryConstant;
import com.spin.techtrove.products.domain.port.CategoryPersistencePort;
import com.spin.techtrove.products.infraestructure.adapter.exception.CategoryException;
import com.spin.techtrove.products.infraestructure.adapter.mapper.CategoryDboMapper;
import com.spin.techtrove.products.infraestructure.adapter.repository.CategoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CategorySpringJpaAdapter implements CategoryPersistencePort {
    private final CategoryRepository categoryRepository;
    private final CategoryDboMapper categoryDboMapper;
    public CategorySpringJpaAdapter(CategoryRepository categoryRepository, CategoryDboMapper categoryDboMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryDboMapper = categoryDboMapper;
    }

    @Override
    public Category create(Category request) {

        var categoryToSave = categoryDboMapper.toDbo(request);
        var categorySaved = categoryRepository.save(categoryToSave);

        return categoryDboMapper.toDomain(categorySaved);

    }

    @Override
    public Category getById(Long id) {
        var optionalCategory = categoryRepository.findById(id);

        if (optionalCategory.isEmpty()){
            throw new CategoryException(HttpStatus.NOT_FOUND, String.format(CategoryConstant.CAT_NOT_FOUND_MESSAGE_ERROR, id));
        }

        return categoryDboMapper.toDomain(optionalCategory.get());
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.findAll()
                .stream()
                .map(categoryDboMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public Category update(Category category) {

        var categoryToUpdate = categoryDboMapper.toDbo(category);
        var categoryUpdated = categoryRepository.save(categoryToUpdate);

        return categoryDboMapper.toDomain(categoryUpdated);

    }

    @Override
    public List<Category> getByIds(List<Long> catIds) {

        return categoryRepository.findByIdIn(catIds)
                .stream()
                .map(categoryDboMapper::toDomain)
                .collect(Collectors.toList());

    }

}
