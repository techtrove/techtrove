package com.spin.techtrove.products.infraestructure.adapter.repository;

import com.spin.techtrove.products.infraestructure.adapter.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {

    List<CategoryEntity> findByIdIn(List<Long> catsIds);

}