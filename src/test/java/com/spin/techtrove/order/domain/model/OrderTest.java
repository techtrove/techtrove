package com.spin.techtrove.order.domain.model;

//import com.spin.techtrove.order.infraestructure.exceptions.InvalidOrderException;
import com.spin.techtrove.orders.domain.model.Order;
import com.spin.techtrove.orders.domain.model.OrderItem;
import com.spin.techtrove.orders.domain.model.Payment;
import com.spin.techtrove.orders.infraestructure.adapter.exception.InvalidOrderException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Disabled
class OrderTest {

    @Test
    void testOrderNumOrder() {
        Order order = new Order();
        order.setId(1L);
        order.setNumOrder("Ord-245");

        assertNotNull(order.getNumOrder());
    }

    @Test
    void testOrderHasItems(){
        Order order = new Order();
        order.setId(1L);

        List<OrderItem> orderItems = new ArrayList<OrderItem>();
        orderItems.add(new OrderItem());
        order.setOrderItems(orderItems);

        assertNotNull(order.getOrderItems());
        assertTrue(order.getOrderItems().size() > 0);
    }

    @Test
    @DisplayName("Probando si la orden tiene tipo de pago.")
    void testOrderHasTypePayment(){
        Order order = new Order();
        order.setId(1L);
        order.setNumOrder("Ord-245");

        Payment tipoPago = new Payment();
        order.setPayment(tipoPago);

        assertNotNull(order.getPayment());
    }

    @Test
    @DisplayName("Valida que las ordenes no sean iguales.")
    void validateDiffOrder() {
        List<OrderItem> orderItems = new ArrayList<OrderItem>();
        orderItems.add(new OrderItem());

        Payment typePayment = new Payment();
        typePayment.setId(1L);
        typePayment.setCode("Debit Card");

        Order orderOne = new Order(1L, "Order-123", new BigDecimal("145.463"), orderItems, typePayment);
        Order orderTwo = new Order(2L, "Order-124", new BigDecimal("4723.1202"), orderItems, typePayment);

        assertNotEquals(orderTwo, orderOne);

    }

    @Test
    @Disabled
    @DisplayName("Valida si el Metodo de Pago es permitido.")
    void validatePaymentAllow() {

        fail();
        List<Payment> allowPayment = new ArrayList<Payment>();
        allowPayment.add(new Payment(1L, "Debit"));
        allowPayment.add(new Payment(2L, "Credit"));

        List<OrderItem> orderItems = new ArrayList<OrderItem>();
        orderItems.add(new OrderItem());

        Payment typePayment = new Payment();
        typePayment.setId(1L);
        typePayment.setCode("Debit");

        Order order = new Order(1L, "Order-123", new BigDecimal("1234.53"),orderItems, typePayment);

        assertTrue(allowPayment.contains(order.getPayment()));
    }

    @Test
    @DisplayName("Valida diferentes casos de las ordenes.")
    void invalidOrder() {
        List<OrderItem> orderItems = new ArrayList<OrderItem>();
        orderItems.add(new OrderItem());

        Payment typePayment = new Payment();
        typePayment.setId(1L);
        typePayment.setCode("Debit");
        Order order = new Order(null, "Order-123", new BigDecimal("1234.34"),orderItems, typePayment);

        assertAll(
                ()->
                    assertThrows(InvalidOrderException.class, order::validateOrder),
                ()->{
                    String exceptionMsg = "El ID de la orden es invalido";
                    String msgEsperado = "El ID de la orden es invalido.";

                    assertEquals(exceptionMsg, msgEsperado);
                }
        );
    }
}