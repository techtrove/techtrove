package com.spin.techtrove.users.domain.model;

import com.spin.techtrove.products.infraestructure.adapter.exception.ProductPriceException;
import com.spin.techtrove.users.infraestructure.adapter.exception.UserException;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

@Disabled
class UserTest {
    User usuario;
    @BeforeAll
    static void beforeAll() {
        System.out.println("Iniciando Test");
    }

    @BeforeEach
    void init(){
        usuario = new User(1L, "JG", "1234", "Jhon", new ArrayList<Rol>());
    }

    @AfterAll
    static void afterAll() {
        System.out.println("Fin Test");
    }

    @Test
    void validaUsuario(){
        assertNotNull(usuario.getUser());
    }

    @Test
    void validaUsuarioUserPass(){
        Exception exception = assertThrows(UserException.class, usuario::validateCodeProduct);
        assertEquals(exception.getMessage(),"Favor de verificar usuario y password");
    }

    @Test
    @DisplayName("Valida Usuario User Pass Dev")
    void validaUsuarioUserPassDev(){
        boolean devEnv = "dev".equals(System.getProperty("ENV"));
        usuario.setUser("");
        assumeTrue(devEnv);
        Exception exception = assertThrows(UserException.class, usuario::validateCodeProduct);
        assertEquals(exception.getMessage(),"Favor de verificar usuario y password");
    }


}