package com.spin.techtrove.productos.domain.model;

import com.spin.techtrove.products.domain.model.Product;
import com.spin.techtrove.products.infraestructure.adapter.exception.InvalidProductException;
import com.spin.techtrove.products.infraestructure.adapter.exception.ProductHasNotCatException;
import com.spin.techtrove.products.infraestructure.adapter.exception.ProductPriceException;
import org.junit.jupiter.api.*;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@Disabled
class ProductTest {
    Product product;
    @BeforeEach
    void init(){
        product = new Product(1L,"SPIN CARD",new BigDecimal("1524.52"),4);
    }

    @AfterEach
    void tearDown() {
        System.out.println("Pruebas finalizadas.");
    }

    @Test
    void testProductCodeNotNull() {

        assertNotNull(product.getCode());
    }

    @Test
    void testProductPriceHigherThenZero() {
        product.setPrice(new BigDecimal("-50.152"));

        Exception exception = assertThrows(ProductPriceException.class, product::validatePrice);
        assertEquals(exception.getMessage(),"El precio del producto no puede ser menor o igual a 0.");

    }

    @Test
    void invalidProductCode() {
        product.setCode("");

        Exception exception = assertThrows(InvalidProductException.class, product::validateCodeProduct);

        String exceptionMsg = exception.getMessage();
        String msgEsperado = "El Codigo del producto es invalido.";

        assertEquals(exceptionMsg, msgEsperado);

    }

    @Test
    void productHasNotCat() {
        product.setCategories(null);

        Exception exception = assertThrows(ProductHasNotCatException.class, product::validateProductCategory);

        String exceptionMsg = exception.getMessage();
        String msgEsperado = "El producto requiere una categoria.";

        assertEquals(exceptionMsg, msgEsperado);

    }



}