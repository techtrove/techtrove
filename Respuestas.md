1. ¿Cómo se aseguró de que su microservicio creciera con la creciente demanda?
En este punto se cubre con el manejo efectivo de colas, el cual se estaría encargando en base a demanda y todos los limites configurados
estarían en un archivo de secreto o REDIS.

2. ¿Qué estrategias se adoptaron o contemplaron para una mantenibilidad duradera?
Se pensó en la estrategia de vertical slicing y screaming, y hacer uso también de circuit breakers, además de cuidado de índices sobre los 
registros insertados en las bases de datos.

3. Narrar una decisión técnica desafiante tomada durante esta tarea y defenderla.
El manejo de datos, aunque pareciera que todo puede definirse como un mismo request/response, definitivamente
al final creo que tener todo ordenado y segregado dependiendo la funcionalidad ayudara a que en un futuro los 
cambios sean mucho más fáciles de hacer y leer, ya que prácticamente con esta arquitectura puedes ir agregando 
más cosas, según sea necesario, sin tocar la lógica de negocio, y si fuera tan grande, es tan fácil como 
desacoplar el dominio y llevarlo a otro proyecto.

4. Como desarrollador senior, ¿cómo garantizaría la calidad del código y las mejores prácticas dentro de un marco de equipo más extenso?
Se instalador diversas herramientas para la verificación del código, además se agregaron algunas pruebas solo en 
el afán de poder marcar un camino de lo que se requiere para mejorar la programación enfocada en la lógica de 
negocio.

5. Describa su enfoque para guiar a un desarrollador junior a comprender y desarrollar este microservicio.
5.1 Se plantea la arquitectura hexagonal con el fin de dejar de lado todas las externalidades.
5.2 Se desacoplan las capas para poder trabajar de forma asilada con cada nivel o capa (infraestructura,
	aplicación y dominio)
5.3 Una vez identificados estos factores puedes determinar las pruebas en base al modelo de negocio, el cual
	va a procurar respetarse desde las pruebas de DDT, esto nos ira marcando la pauta para poder mejorar 
	nuestro código.
5.4 Desde de infraestructura hasta el dominio, la aplicación fluye y es más fácil comprender en donde se hará uso
	de cada componente, entender esto, ayudara a mejorar el código y a bajar el % de errores cuando se
	programa.

6. ¿Cómo explicaría la importancia de una decisión técnica particular a una parte interesada o miembro del equipo no técnico?
	Existe diferentes factores para poder explicar la necesidad del negocio desde una parte no técnica, por ejemplo
	dinero, tiempo, servicio, calidad, rapidez de la aplicación, etc. aunque nada de esto es tan tangible, podemos
	ejemplificar casos donde el cliente puede llegar a tener una mala experiencia, porque el servicio no se lo pudimos dar,
	otro ejemplo serio a que una mala construcción de nuestra aplicación, nos puede llevar a que el servicio sea lento
	o de mala calidad.


7. Aquí hay un Dockerfile básico para una aplicación Java:
¿Puede proporcionar algunas modificaciones o comentarios para mejorar o describir el Dockerfile?
    
    ```estibador
    DESDE openjdk:11
    COPIAR ./target/my-app.jar /usr/src/
    DIRTRABAJO /usr/aplicación
    CMD ["java", "-jar", "mi-aplicación.jar"]
    ```
    
    **Tareas**:
    
    - Identifique y solucione cualquier problema que vea en Dockerfile.
	* Lo cambiaria por la forma siguiente:
		FROM openjdk:11
		RUN mkdir -p /usr/aplicación
		WORKDIR /usr/aplicación
		COPY ./target/my-app.jar /usr/src/
		RUN yarn install --production=true --> Eso para poder ignorar todo lo que no se debe instalar de otros ambientes.
		CMD ["java", "-jar", "my-app.jar"]
		EXPOSE 3131 -> Ejemplo en que puerto será expuesto para poder acceder.
	
	
    - Agregue comentarios para describir cada paso del Dockerfile para hacerlo más claro.
		* Generar una imagen desde una versión de OPENJDK 11
		* Set de mi ruta de trabajo.
		* Copiar el JAR de la ruta target a la ruta /usr/src/
		* Instalar 
	
8. En Kubernetes, es vital no codificar datos confidenciales dentro de aplicaciones o configuraciones. Describa cómo administraría y recuperaría información confidencial, 
   como contraseñas de bases de datos o claves API, en un entorno de Kubernetes sin codificarlas en su aplicación.
   R: Podemos definir variables de ambiente en los archivos de env o envFrom, dependiendo de la estrategia que se busque seguir, variables con valor
   o un mapa de configuración para algo en específico.
